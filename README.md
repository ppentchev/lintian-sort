The lintian-sort tool - reproducibly sort the Lintian tool's output
===================================================================

Description
-----------

The lintian-sort tool reorders the messages reported by the lintian(1)
Debian package analysis tool so that they are kept in the same order
between successive builds.  This minimizes the changes that the package
maintainer sees using a file comparison tool (like diff(1)) to check if
any new problems have appeared or any of the old ones have been fixed.

The lintian-sort tool keeps any additional information (lines starting
with "N:") together with the tags they describe; however, it takes care to
put the final "N: 5 tags overridden" message (along with any follow-up
descriptive lines) at the end.

Obtaining lintian-sort
----------------------

The latest version of the lintian-sort tool is available from its
webpage:

  https://devel.ringlet.net/textproc/lintian-sort/

It is developed in a GitLab repository:

  https://gitlab.com/ppentchev/lintian-sort

Contacting the author
---------------------

The lintian-sort tool is developed by Peter Pentchev <roam@ringlet.net>.
