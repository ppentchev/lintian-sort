Change log for lintian-sort
===========================

0.2.0 (2018-08-14)
------------------

- Catch up with the Perl 6 Getopt::Std 1.x API.
- Actually fail if a test fails!  Adds a dependency on the "prove"
  tool found in Perl 5.
- Add tests for lintian-sort -V and -h.
- Add the --features command-line option.
- Reformat the uses of ?: in the source.

0.1.0 (2016-11-08)
------------------

- Initial version.

Comments: Peter Pentchev <roam@ringlet.net>
