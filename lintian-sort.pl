#!/usr/bin/perl
#
# Copyright (c) 2016, 2018  Peter Pentchev
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
# OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
# OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.

use v5.10;
use strict;
use warnings;

package Cfg;

use v5.10;
use strict;
use warnings;

use Moo;
use namespace::clean;

has debug => (
	is => 'ro',
);

has files => (
	is => 'ro',
);

package Line;

use v5.10;
use strict;
use warnings;

use Moo;
use namespace::clean;

with 'MooX::Role::CloneSet';

has line => (
	is => 'ro',
);

has key => (
	is => 'ro',
);

has is_new => (
	is => 'ro',
);

has type => (
	is => 'ro',
);

has pkg => (
	is => 'ro',
);

has tag => (
	is => 'ro',
);

has info => (
	is => 'ro',
);

has desc => (
	is => 'ro',
);

# The "type_priority" and "is_binary_pkg" properties are used internally for sorting.
has type_priority => (
	is => 'ro',
);

has is_binary_pkg => (
	is => 'ro',
);

my %type_prio = (
	E => 0,
	W => 1,
	I => 2,
	P => 3,
	X => 4,
	N => 5,
);

sub sort_prep($)
{
	my ($self) = @_;

	$self->cset(
		type_priority => defined $self->type
			? $type_prio{$self->type}
			: $self->type_priority,
		is_binary_pkg => defined $self->pkg && $self->pkg !~ /\ssource$/ ? 1 : 0,
	)
}

sub sort_cmp($ $)
{
	my ($self, $other) = @_;

	$self->is_binary_pkg <=> $other->is_binary_pkg ||
	$self->pkg cmp $other->pkg ||
	$self->type_priority <=> $other->type_priority ||
	$self->tag cmp $other->tag ||
	$self->key cmp $other->key
}

package Output;

use v5.10;
use strict;
use warnings;

use Moo;
use namespace::clean;

has status => (
	is => 'ro',
);

has text => (
	is => 'ro',
);

package main;

use v5.14;

use Getopt::Std;
use List::Util qw(max);
use Path::Tiny;
use Regexp::Grammars;

use constant usage_message => <<'EOUSAGE' =~ s/\n$//r;
Usage:	lintian-sort [-v] filename...
	lintian-sort -V | -h

	-h	display program usage information and exit
	-V	display program version information and exit
	-v	verbose operation; display diagnostic output

The standard input may be specified as '-'.
EOUSAGE

use constant version_string => '0.2.0';
use constant version_message => 'lintian-sort '.version_string;
use constant features_message =>
	'Features: lintian-sort='.version_string;

use constant re_override => qr/^
	(?<type>
		N
	):
	\s+ \d+ \s+
	(?<tag>
		tags? \s+ overridden
	)
	(?:
		\s+
		(?<info>
			.*
		)
	)?
	$/x;

use constant re_tag_desc => qr/^
	(?<type>
		N
	):
	(?:
		\s+
		(?<info>
			.*
		)
	)?
	$/x;

use constant re_tag_line => qr/
	(?<type>
		[EIPWX]
	):
	\s+
	(?<pkg>
		[^:]+
	):
	\s+
	(?<tag>
		\S+
	)
	(?:
		\s+
		(?<info>
			.+
		)
	)?
	$/x;

sub err($)
{
	Output->new(status => 1, text => $_[0])
}

sub ok($)
{
	Output->new(status => 0, text => $_[0])
}

sub version()
{
	ok(version_message)
}

sub features()
{
	ok(features_message)
}

sub usage(;$)
{
	my $err = $_[0] // 1;

	$err ? err(usage_message) : ok(usage_message)
}

sub parse_options($ $ $)
{
	my ($opts, $args, $callback) = @_;

	@{$args}
		? $callback->(Cfg->new(
			debug => $opts->{v} ? sub { say STDERR "RDBG $_[0]" } : sub {},
			files => [ @{$args} ],
		))
		: (
			err('No filenames to process'),
			usage
		)
}

my $grammar = qr{
	^ <[Block]>* $

	<token: Block>	<Line> <[Desc]>*

	<token: Line>	<Type> : [ ] <Package> : [ ] <Tag> [ ]? <Info> \n

	<token: Desc>	N: [ ]? <Info> \n

	<token: Type>	[EIPWX]

	<token: Package>[^:\n]+

	<token: Tag>	\S+

	<token: Info>	[^\n]*
}xs;

sub process_single_file($ $)
{
	my ($cfg, $fname) = @_;
	my $f;

	no warnings 'recursion';

	my $refit_tag_desc;
	$refit_tag_desc = sub {
		my ($tags, $desc, $accum, $seen, $cb) = @_;

		@{$tags}
			? do {
				my ($first, @rest) = @{$tags};
				my $tag = $first->tag;

				$seen->{$tag}
					? $refit_tag_desc->(
						\@rest,
						$desc, [
							@{$accum},
							$first->line,
						],
						$seen,
						$cb)
					: $refit_tag_desc->(
						\@rest,
						$desc,
						[
							@{$accum},
							$first->line . "\n" . $desc->{$tag},
						],
						{
							%{$seen},
							$tag => 1,
						},
						$cb)
			}
			: $cb->($accum)
	};

	my $finalize = sub {
		my ($data, $epilogue) = @_;

		my @sorted = sort { $a->sort_cmp($b) } map { $_->sort_prep } @{$data};
		$cfg->debug->('Done, stored '.scalar(@sorted).' tags');

		my %extr = map {
			length($_->desc) ? ($_->tag => $_->desc) : ()
		} @sorted;
		$cfg->debug->('Extracted descriptions for '.scalar(keys %extr).' tags');

		my %desc = map {
			my $t = $_->tag;

			$t => $extr{$t} // ''
		} @sorted;
		$refit_tag_desc->(\@sorted, \%desc, [], {}, sub {
			ok(
				join("\n", @{$_[0]}) .
				$epilogue
			)
		})
	};

	my $parse = sub {
		my ($contents, $epilogue) = @_;

		$contents =~ /$grammar/
			? do {
				my @data = map {
					my $li = $_->{Line};
					my $line = $li->{''} =~ s/\n$//r;
					my $desc = $_->{Desc} // [];
					Line->new(
						line => $line,
						key => $line,

						type => $li->{Type},
						pkg => $li->{Package},
						tag => $li->{Tag},
						info => $li->{Info},

						desc => @{$desc}
							? join("\n", map {
								$_->{''} =~ s/\n$//r,
							} @{$desc})
							: undef,
					)
				} @{$/{Block}};

				$finalize->(\@data, $epilogue)
			}
			: err("Could not parse '$fname' as Lintian output")
	};

	my $get_file_contents = sub {
		my ($cb) = @_;

		$fname eq '-'
			? do {
				my $contents;
				{
					local $/;
					$contents = <STDIN>;
				}
				$cb->($contents)
			}
			: do {
				my $contents = eval { path($fname)->slurp };
				$@?
					err("Could not open '$fname': $@"):
					$cb->($contents)
			}
	};

	my $split_epilogue = sub {
		my ($contents, $cb) = @_;

		$contents =~ /^N: \d+ tags? overridden.*/ms?
			do {
				my $ep = $& =~ s/\n$//r;

				$cb->(substr($contents, 0, length($contents) - length($ep) - 1), "\n".$ep)
			}:
			$cb->($contents, '')
	};

	$get_file_contents->(sub {
		$split_epilogue->($_[0], sub {
			$parse->($_[0], $_[1])
		})
	})
}

sub process_files($)
{
	my ($cfg) = @_;

	map { process_single_file($cfg, $_) } @{$cfg->files};
}

sub get_options($)
{
	my ($callback) = @_;

	my %opts;
	getopts('hVv-:', \%opts)?
		$callback->(\%opts, \@ARGV):
		usage
}

sub parse_opts_ver_help($ $ $)
{
	my ($opts, $args, $callback) = @_;
	my $has_dash = defined $opts->{'-'};
	my $dash_help = $has_dash && $opts->{'-'} eq 'help';
	my $dash_version = $has_dash && $opts->{'-'} eq 'version';
	my $dash_features = $has_dash && $opts->{'-'} eq 'features';
	my @msg = (
		($opts->{V} || $dash_version ? version : ()),
		(              $dash_features ? features : ()),
		($opts->{h} || $dash_help ? usage 0 : ()),
	);

	$has_dash && !$dash_help && !$dash_version && !$dash_features
		? err("Invalid long option '".$opts->{'-'}."' specified")
		: @msg
			? @msg
			: $callback->($opts, $args)
}

sub display_and_exit(@)
{
	my @res = @_;
	my @err = map $_->text, grep $_->status, @res;
	my @ok = map $_->text, grep !$_->status, @res;

	say STDERR $_ for @err;
	say $_ for @ok;
	exit max map $_->status, @res;
}

MAIN:
{
	display_and_exit get_options sub {
		# opts, args -- output
		parse_opts_ver_help $_[0], $_[1], sub {
			# opts, args -- output
			parse_options $_[0], $_[1], sub {
				# cfg -- output
				process_files $_[0]
			}
		}
	};
}
