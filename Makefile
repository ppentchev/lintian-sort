#!/usr/bin/make -f
#
# Copyright (c) 2016, 2018  Peter Pentchev
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
# OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
# OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.

PROG=		lintian-sort
MAN1=		lintian-sort.1

PROG_PERL5=	lintian-sort.pl
PROG_PERL6=	lintian-sort.p6

PROG_USED?=	${PROG_PERL5}

PREFIX?=	/usr/local
BINDIR?=	${PREFIX}/bin
MANDIR?=	${PREFIX}/share/man
MAN1DIR?=	${MANDIR}/man1

BINOWN?=	root
BINGRP?=	root
BINMODE?=	755

SHAREOWN?=	${BINOWN}
SHAREGRP?=	${BINGRP}
SHAREMODE?=	${BINMODE}

MAN1Z=		${MAN1}.gz

GROFF?=		groff

GZIP?=		gzip
GZIP_N?=	-n

INSTALL?=	install
INSTALL_SCRIPT?=	${INSTALL} -o ${BINOWN} -g ${BINGRP} -m ${BINMODE}
INSTALL_DATA?=		${INSTALL} -o ${SHAREOWN} -g ${SHAREGRP} -m ${SHAREMODE}

MKDIR?=		mkdir -p

RM?=		rm -f

all:		${PROG} ${MAN1Z}

${PROG}:	${PROG_USED}
		[ "${PROG_USED}" = "${PROG}" ] || ${INSTALL} -m '${BINMODE}' -- '${PROG_USED}' '${PROG}'

${MAN1Z}:	${MAN1}
		${GZIP} -c ${GZIP_N} -- '${MAN1}' > '${MAN1Z}' || (${RM} -- '${MAN1Z}'; false)

install:
		${MKDIR} -- '${DESTDIR}${BINDIR}'
		${INSTALL_SCRIPT} -- '${PROG}' '${DESTDIR}${BINDIR}/'
		
		${MKDIR} -- '${DESTDIR}${MAN1DIR}'
		${INSTALL_DATA} -- '${MAN1Z}' '${DESTDIR}${MAN1DIR}/'

clean:
		${RM} -- '${PROG}' '${MAN1Z}'

test:		all
		${MAKE} test-single TEST_PROG='./${PROG}'
		${MAKE} test-manpage
		
test-manpage:	${MAN1}
		${GROFF} -z -mdoc -w w -- '${MAN1}'

test-all:	${PROG_PERL5} ${PROG_PERL6}
		set -e; for f in '${PROG_PERL5}' '${PROG_PERL6}'; do \
			${MAKE} test-single TEST_PROG="./$$f"; \
		done
		${MAKE} test-manpage

test-single:
		env LINTIAN_SORT='${TEST_PROG}' prove -v ./t/run-tests.sh

.PHONY:		all install clean test test-all test-single
