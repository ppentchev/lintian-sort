#!/usr/bin/env perl6
#
# Copyright (c) 2016, 2018  Peter Pentchev
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
# OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
# OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.

use v6.c;

use Getopt::Std;

constant usage-message =
｢Usage:	lintian-sort [-v] filename...
	lintian-sort -V | -h

	-h	display program usage information and exit
	-V	display program version information and exit
	-v	verbose operation; display diagnostic output

The standard input may be specified as '-'.｣;

constant version-string = '0.2.0';
constant version-message = 'lintian-sort ' ~ version-string;
constant features-message =
	'Features: lintian-sort=' ~ version-string;

class Output
{
	has UInt:D $.status is required;
	has Str:D $.text is required;
}

class Cfg
{
	has Block:D $.debug is required;
	has Str:D @.files is required;
}

class Line
{
	has Str $.line;
	has Str $.key;

	has Str $.type;
	has Str $.tag;
	has Str $.pkg;
	has Str $.info;

	has Str $.desc;

	has UInt $.type-priority;
	has UInt $.is-binary-pkg;

	method cset(*%args)
	{
		Line.new(
			:line($!line),
			:key($!key),

			:type($!type),
			:tag($!tag),
			:pkg($!pkg),
			:info($!info),

			:desc($!desc),

			:type-priority($!type-priority),
			:is-binary-pkg($!is-binary-pkg),

			|%args
		)
	}

	my %type-prio = (
		:E(0),
		:W(1),
		:I(2),
		:P(3),
		:X(4),
		:N(5),
	);

	method sort-prep()
	{
		self.cset(
			type-priority => $!type.defined
				?? %type-prio{$!type}
				!! UInt,
			is-binary-pkg => $!pkg.defined && $!pkg !~~ / \s source $ /
				?? 1
				!! 0,
		)
	}

	method sort-cmp(Line:D $other)
	{
		$!is-binary-pkg <=> $other.is-binary-pkg ||
		$!pkg cmp $other.pkg ||
		$!type-priority <=> $other.type-priority ||
		$!tag cmp $other.tag ||
		$!key cmp $other.key
	}
}

grammar LintianGrammar
{
	token TOP { ^ <block>* $ };

	token block { <tag-line> <tag-desc>* };

	token tag-line { <type> ': ' <pkg> ': ' <tag> <info> };
	token tag-desc { <type-N> ':' <info> };

	token type { <[EIPWX]> };
	token type-N { 'N' };

	token pkg { <-[:\n]>+ };
	token tag { \S+ };
	token info { [ ' ' <info-contents> ]? \n };
	token info-contents { <-[\n]>* };
}

class LintianActions
{
	method TOP($/) {
		make $<block>».made;
	}

	method block($/) {
		my $desc = $<tag-desc> ?? $<tag-desc>».made.join("\n") !! Str;
		my %attr = $<tag-line>.made;
		make Line.new(
			:desc($desc),
			|%attr
		)
	}

	method tag-line($/) {
		my $line = ~$/.chomp;
		my %attr =
			:line($line),
			:key($line),

			:type(~$<type>),
			:pkg(~$<pkg>),
			:tag(~$<tag>),
			:info($<info>.made);
		make %attr
	}

	method tag-desc($/) {
		make ~$/.chomp
	}

	method info($/) {
		make $<info-contents> ?? ~$<info-contents> !! Str
	}
}

sub err(Str:D $text)
{
	Output.new(:status(1), :text($text))
}

sub ok(Str:D $text)
{
	Output.new(:status(0), :text($text))
}

sub usage(Bool:D $err = True)
{
	[Output.new(:status($err ?? 1 !! 0), :text(usage-message))]
}

sub version()
{
	[ok(version-message)]
}

sub features()
{
	[ok(features-message)]
}

sub get-options(Block:D $callback)
{
	my %opts = getopts('hVv-:', @*ARGS);
	CATCH { when X::Getopt::Std { .message.note; usage } };
	$callback(%opts, @*ARGS)
}

sub display-and-exit(@res)
{
	my %by-status = @res.classify(*.status == 0);

	(%by-status{False} // ()).map(*.text)».note;
	(%by-status{True} // ()).map(*.text)».say;
	exit @res.map(*.status).max;
}

sub parse-opts-ver-help(%opts, @args, Block:D $callback)
{
	my Bool:D $has-dash = ?%opts{'-'};
	my Bool:D $dash-help = $has-dash && %opts{'-'} eq 'help';
	my Bool:D $dash-version = $has-dash && %opts{'-'} eq 'version';
	my Bool:D $dash-features = $has-dash && %opts{'-'} eq 'features';
	my Output:D @msg =
		|(?%opts<V> || $dash-version  ?? version() !! ()),
		|(             $dash-features ?? features() !! ()),
		|(?%opts<h> || $dash-help     ?? usage(False) !! ()),
	;

	$has-dash && !$dash-help && !$dash-version && !$dash-features
		?? [err("Invalid long option '" ~ %opts{'-'} ~ "' specified")]
		!! @msg
			?? @msg
			!! $callback(%opts, @args)

}

sub parse-options(%opts, @args, Block:D $callback)
{
	@args
		?? $callback(Cfg.new(
			:debug(?%opts<v>
				?? -> Str:D $msg { note "RDBG $msg" }
				!! -> Str:D $msg {}
			),
			:files(@args),
		))
		!! [
			err('No filenames to process'),
			|usage
		]
}

sub process-single-file(Cfg:D $cfg, $fname)
{
	sub refit-tag-desc(@tags, %descr, @accum, %seen, Block:D $cb)
	{
		@tags
			?? do {
				my ($first, @rest) = @tags;
				my $tag = $first.tag;

				?%seen{$tag}
					?? refit-tag-desc(
						@rest,
						%descr,
						[
							|@accum,
							$first.line,
						],
						%seen,
						$cb
					)
					!! refit-tag-desc(
						@rest,
						%descr,
						[
							|@accum,
							$first.line ~ "\n" ~ %descr{$tag},
						],
						{
							%seen,
							$tag => True,
						},
						$cb
					)
			}
			!! $cb(@accum)
	}

	sub parse(Str:D $contents, Str:D $epilogue)
	{
		my $m = LintianGrammar.new.parse($contents, :actions(LintianActions.new));
		$m
			?? do {
				my @sorted = $m.made».sort-prep.sort({ $^a.sort-cmp($^b) });
				$cfg.debug()("Done, sorted @sorted.elems() tags");


				my %extr = @sorted.flatmap: {
					$_.desc ?? ($_.tag => $_.desc) !! ()
				};
				$cfg.debug()("Extracted descriptions for %extr.elems() tags");

				my %desc = @sorted.map: {
					my $t = $_.tag;

					$t => %extr{$t} // ''
				};

				refit-tag-desc(@sorted, %desc, [], {}, -> @res {
					ok(@res.join("\n") ~ $epilogue)
				})
			}
			!! err("Could not parse '$fname' as Lintian output")
	}

	sub get-file-contents(Block:D $cb)
	{
		$fname eq '-'
			?? $cb($*IN.slurp-rest)
			!! do {
				my $contents = try $fname.IO.slurp;

				$contents.defined
					?? $cb($contents)
					!! err("Could not open $fname: $!")
			}
	}

	sub split-epilogue(Str:D $contents, Block:D $cb)
	{
		$contents ~~ / ^^ $<epilogue> = [ 'N: ' \d+ ' tag' 's'? ' overridden' .* ] $ /
			?? do {
				my $ep = ~$/<epilogue>;
				$cb($contents.substr(0, * - $ep.chars), "\n" ~ $ep.chomp)
			}
			!! $cb($contents, '')
	}

	get-file-contents(-> Str:D $contents {
		split-epilogue($contents, -> Str:D $real-contents, Str:D $epilogue {
			parse($real-contents, $epilogue)
		})
	})
}

sub process-files(Cfg:D $cfg)
{
	$cfg.files.map: { process-single-file($cfg, $_) }
}

{
	display-and-exit(get-options(-> %opts, @args {
		parse-opts-ver-help(%opts, @args, -> %opts, @args {
			parse-options(%opts, @args, -> Cfg:D $cfg {
				process-files($cfg)
			})
		})
	}))
}
