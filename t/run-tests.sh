#!/bin/sh
#
# Copyright (c) 2016, 2018  Peter Pentchev
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
# OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
# OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.

set -e

if [ -f 'tap-functions.sh' ]; then
	. tap-functions.sh
elif [ -f 't/tap-functions.sh' ]; then
	. t/tap-functions.sh
else
	echo 'Bail out! Could not find tap-functions.sh'
	exit 99
fi

# Which program do we need to check?
: "${LINTIAN_SORT:=./lintian-sort.pl}"

# What's the interpreter used by this program?
if head -n1 -- "$LINTIAN_SORT" | fgrep -qwe perl6; then
	run='perl6'
elif head -n1 -- "$LINTIAN_SORT" | fgrep -qwe perl; then
	run='perl'
else
	bailout_ "Could not determine the interpreter needed to run '$LINTIAN_SORT'"
fi

tempf="$(mktemp test-output.txt.XXXXXX)"
trap "rm -f -- '$tempf'" EXIT HUP INT TERM QUIT
tempdiff="$(mktemp test-diff.txt.XXXXXX)"
trap "rm -f -- '$tempf' '$tempdiff'" EXIT HUP INT TERM QUIT

count="$(ls t/data/*-unsorted.txt | wc -l)"
plan_ "$(expr 4 '*' "$count" + 15)"

if "$run" "$LINTIAN_SORT" -V > "$tempf" 2> "$tempdiff"; then
	ok_ "$LINTIAN_SORT -V succeeded"
else
	not_ok_ "$LINTIAN_SORT -V succeeded"
fi
if [ "$(wc -l < "$tempf" | tr -d ' ')" = 1 ]; then
	ok_ "$LINTIAN_SORT -V output a single line"
else
	not_ok_ "$LINTIAN_SORT -V output a single line"
fi
if [ ! -s "$tempdiff" ]; then
	ok_ "$LINTIAN_SORT -V did not output any error messages"
else
	not_ok_ "$LINTIAN_SORT -V did not output any error messages"
fi

if "$run" "$LINTIAN_SORT" -h > "$tempf" 2> "$tempdiff"; then
	ok_ "$LINTIAN_SORT -h succeeded"
else
	not_ok_ "$LINTIAN_SORT -h succeeded"
fi
if [ -s "$tempf" ]; then
	ok_ "$LINTIAN_SORT -h output something"
else
	not_ok_ "$LINTIAN_SORT -h output something"
fi
if [ ! -s "$tempdiff" ]; then
	ok_ "$LINTIAN_SORT -h did not output any error messages"
else
	not_ok_ "$LINTIAN_SORT -h did not output any error messages"
fi

if "$run" "$LINTIAN_SORT" --version > "$tempf" 2> "$tempdiff"; then
	ok_ "$LINTIAN_SORT --version succeeded"
else
	not_ok_ "$LINTIAN_SORT --version succeeded"
fi
if [ "$(wc -l < "$tempf" | tr -d ' ')" = 1 ]; then
	ok_ "$LINTIAN_SORT --version output a single line"
else
	not_ok_ "$LINTIAN_SORT --version output a single line"
fi
if [ ! -s "$tempdiff" ]; then
	ok_ "$LINTIAN_SORT --version did not output any error messages"
else
	not_ok_ "$LINTIAN_SORT --version did not output any error messages"
fi

if "$run" "$LINTIAN_SORT" --help > "$tempf" 2> "$tempdiff"; then
	ok_ "$LINTIAN_SORT --help succeeded"
else
	not_ok_ "$LINTIAN_SORT --help succeeded"
fi
if [ -s "$tempf" ]; then
	ok_ "$LINTIAN_SORT --help output something"
else
	not_ok_ "$LINTIAN_SORT --help output something"
fi
if [ ! -s "$tempdiff" ]; then
	ok_ "$LINTIAN_SORT --help did not output any error messages"
else
	not_ok_ "$LINTIAN_SORT --help did not output any error messages"
fi

if "$run" "$LINTIAN_SORT" --features > "$tempf" 2> "$tempdiff"; then
	ok_ "$LINTIAN_SORT --features succeeded"
else
	not_ok_ "$LINTIAN_SORT --features succeeded"
fi
if egrep -qe '^Features:.* lintian-sort=[0-9][0-9A-Za-z.]+( |$)' -- "$tempf"; then
	ok_ "$LINTIAN_SORT --features output a features line"
else
	not_ok_ "$LINTIAN_SORT --features output a features line"
fi
if [ ! -s "$tempdiff" ]; then
	ok_ "$LINTIAN_SORT --features did not output any error messages"
else
	not_ok_ "$LINTIAN_SORT --features did not output any error messages"
fi

for un in t/data/*-unsorted.txt; do
	sr="${un%-unsorted.txt}-sorted.txt"

	if "$run" "$LINTIAN_SORT" "$un" > "$tempf" 2> "$tempdiff"; then
		ok_ "$LINTIAN_SORT $un succeeded"
		if diff -q -- "$sr" "$tempf" > "$tempdiff"; then
			ok_ "$LINTIAN_SORT $un produced the correct output"
		else
			not_ok_ "$LINTIAN_SORT $un did not produce the correct output"
			cat -- "$tempdiff" 1>&2
		fi
	else
		not_ok_ "$LINTIAN_SORT $un failed"
		cat -- "$tempdiff" 1>&2
		skip_
	fi

	if "$run" "$LINTIAN_SORT" - < "$un" > "$tempf" 2> "$tempdiff"; then
		ok_ "$LINTIAN_SORT - < $un succeeded"
		if diff -q -- "$sr" "$tempf" > "$tempdiff"; then
			ok_ "$LINTIAN_SORT - < $un produced the correct output"
		else
			not_ok_ "$LINTIAN_SORT - < $un did not produce the correct output"
			cat -- "$tempdiff" 1>&2
		fi
	else
		not_ok_ "$LINTIAN_SORT - < $un failed"
		cat -- "$tempdiff" 1>&2
		skip_
	fi
done
