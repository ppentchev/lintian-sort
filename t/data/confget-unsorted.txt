P: confget source: package-uses-experimental-debhelper-compat-version 10
N: 
N:    The debhelper compatibility version used by this package is marked as
N:    experimental by the debhelper developer. You may consider using a stable
N:    compatibility version.
N:    
N:    The compatibility version can be set in (preferred) debian/compat or by
N:    setting and exporting DH_COMPAT in debian/rules. If it is not set in
N:    either place, debhelper defaults to the deprecated compatibility version
N:    1.
N:    
N:    Refer to the debhelper(7) manual page for details.
N:    
N:    Severity: pedantic, Certainty: certain
N:    
N:    Check: debhelper, Type: source
N: 
W: confget source: newer-standards-version 3.9.8 (current is 3.9.7)
N: 
N:    The source package refers to a Standards-Version which is newer than the
N:    highest one lintian is programmed to check. If the source package is
N:    correct, then please upgrade lintian to the newest version. (If there is
N:    no newer lintian version, then please bug lintian-maint@debian.org to
N:    make one.)
N:    
N:    Severity: normal, Certainty: certain
N:    
N:    Check: standards-version, Type: source
N: 
N: 1 tag overridden (1 warning)
