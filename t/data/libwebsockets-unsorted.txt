P: libwebsockets source: debian-watch-may-check-gpg-signature
N: 
N:    This watch file does not include a means to verify the upstream tarball
N:    using cryptographic signature.
N:    
N:    If upstream distributions provide such signatures, please use the
N:    pgpsigurlmangle options in this watch file's opts= to generate the URL
N:    of an upstream GPG signature. This signature is automatically downloaded
N:    and verified against a keyring stored in
N:    debian/upstream/signing-key.asc.
N:    
N:    Of course, not all upstreams provide such signatures, but you could
N:    request them as a way of verifying that no third party has modified the
N:    code against their wishes after the release. Projects such as
N:    phpmyadmin, unrealircd, and proftpd have suffered from this kind of
N:    attack.
N:    
N:    Refer to the uscan(1) manual page for details.
N:    
N:    Severity: pedantic, Certainty: certain
N:    
N:    Check: watch-file, Type: source
N: 
W: libwebsockets-test-server: binary-without-manpage usr/bin/libwebsockets-test-client
N: 
N:    Each binary in /usr/bin, /usr/sbin, /bin, /sbin or /usr/games should
N:    have a manual page
N:    
N:    Note that though the man program has the capability to check for several
N:    program names in the NAMES section, each of these programs should have
N:    its own manual page (a symbolic link to the appropriate manual page is
N:    sufficient) because other manual page viewers such as xman or tkman
N:    don't support this.
N:    
N:    If the name of the man page differs from the binary by case, man may be
N:    able to find it anyway; however, it is still best practice to make the
N:    case of the man page match the case of the binary.
N:    
N:    If the man pages are provided by another package on which this package
N:    depends, lintian may not be able to determine that man pages are
N:    available. In this case, after confirming that all binaries do have man
N:    pages after this package and its dependencies are installed, please add
N:    a lintian override.
N:    
N:    Refer to Debian Policy Manual section 12.1 (Manual pages) for details.
N:    
N:    Severity: normal, Certainty: possible
N:    
N:    Check: manpages, Type: binary
N: 
W: libwebsockets-test-server: binary-without-manpage usr/bin/libwebsockets-test-echo
W: libwebsockets-test-server: binary-without-manpage usr/bin/libwebsockets-test-fraggle
W: libwebsockets-test-server: binary-without-manpage usr/bin/libwebsockets-test-fuzxy
W: libwebsockets-test-server: binary-without-manpage usr/bin/libwebsockets-test-ping
W: libwebsockets-test-server: binary-without-manpage usr/bin/libwebsockets-test-server
W: libwebsockets-test-server: binary-without-manpage usr/bin/libwebsockets-test-server-extpoll
W: libwebsockets-test-server: binary-without-manpage usr/bin/libwebsockets-test-server-libev
W: libwebsockets-test-server: binary-without-manpage usr/bin/libwebsockets-test-server-libuv
W: libwebsockets-test-server: binary-without-manpage usr/bin/libwebsockets-test-server-pthreads
I: libwebsockets-test-server: unused-override possible-gpl-code-linked-with-openssl
N: 
N:    Lintian discovered an unused override entry in its database. Most likely
N:    it was used for a false-positive that has been fixed. However, some tags
N:    are only triggered in packages built on certain architectures. In this
N:    case, the override may need an architecture qualifier.
N:    
N:    If the override is unused, please remove it from the overrides file.
N:    
N:    Refer to Lintian User's Manual section 2.4.3 (Architecture specific
N:    overrides) for details.
N:    
N:    Severity: wishlist, Certainty: certain
N: 
I: libwebsockets8: unused-override possible-gpl-code-linked-with-openssl
